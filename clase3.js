
function prueba (parametro1, parametro2){
    console.log('parametro1 : '+parametro1);
    console.log('parametro2 : '+parametro2);
}

//prueba('Parametro1','Parametro2')

const prueba=function (mensaje){
    console.log('Mensaje '+mensaje);
}

const prueba2=(params)=>{
    console.log(params);
}

const prueba3=params=>{
    console.log(params);
}

const prueba4=parametros=> console.log(parametros);


function prueba5(parametro){
    return parametro;
}

const ejecutar=unaFuncion=>unaFuncion()
const saludar=()=>console.log('saludos');

const action=()=>console.log('Ha ejecutado una funcion');
/*setTimeout(action,2000);*/


function sumar(a,b){
    let c=Number(a)+Number(b);
    return console.log('El resultado de la suma de ${a} + ${b} es:' +c);
}
function restar(a,b){
    let c=Number(a)-Number(b);
    return console.log('El resultado de la resta de ${a} + ${b} es:' +c);
}

function multiplicar(a,b){
    let c=Number(a)*Number(b);
    return console.log('El resultado de la multiplicacion de ${a} + ${b} es:' +c);
}

function dividir(a,b){
    let c=Number(a)/Number(b);
    return console.log('El resultado de la division de ${a} + ${b} es:' +c);
}


const miOperacion= operacion=>operacion();

miOperacion(sumar(1,2));
miOperacion(restar(1,2));
miOperacion(multiplicar(1,2));
miOperacion(dividir(1,2));

/*fetch('/miEndpoint')
    .then(function (response){
        //Aca definimos toda la logica si esta todo ok
    }).catch(function (error){
        //Aca definimos toda la logica de error
    })
    .finally(function (response){
       // Aca definimos si esta todo okey o no
 });*/

async function funcionAsincronica(){
    return 'algo';
}

const miAlgoRetornado= await funcionAsincronica();

setTimeout(function(){
    // aca definimos que instrucciones queremos desarrollar
},2000);

setInterval(function (){
    // aca definimos cada intervalo de tiempo repetir
    // instrucciones
},2000)


async function miFuncionAsincronica(mensaje){
    console.log('El mensaje: '+mensaje);
}

await  miFuncionAsincronica('Mi mensaje');