const fs=require('fs');
const stream = require("stream");

//CRUD ( crear, leer ,actualizar, eliminar)

const perros=[{
    type:'animal'
}]

//fs.writeFileSync('./perros.json',JSON.stringify(perros));

//Crear o escribir

/*let data=fs.readFileSync('./perros.json','utf-8');
let misDatos=JSON.parse(data);
console.log(misDatos);*/

//Eliminar
//fs.unlinkSync('./perros.json');


//Crear directorio
//fs.mkdirSync('./prueba');

//Eliminar directorio
//fs.rmdirSync('./prueba');

/*fs.writeFile('nuevo.txt',"Primera linea", function (err){
     if (err){
         return console.log(err);
     }

    console.log('El archivo fue creado correctamente');
});*/







//el fs.stat se usa para verificar si un archivo o una carpeta
//existen

/*fs.stat('nuevo.txt',function (err){
        console.log(err.code);
    if(err ==null){
        console.log('El archivo existe');
    }else if(err.code =='ENOENT'){
        console.log('el archivo no existe');
    }else{
        console.log(err);
    }
});*/

//el fs.rename se usa para renombrar un archivo

/*fs.rename('./nuevo.txt','./nuevoV2.txt',(err)=>{
    if(err) throw err;
    console.log('El archivo fue renombrado');
});*/

// Esto sirve para el manejo de exepciones en caso
// de que quieran saber si un archivo existe o no

/*try{
    const data=fs.readFileSync('./nuevoV2.txt');
    console.log('existe el archivo');
}catch (err){
    console.log(err);
}*/

//Forma de Eliminar Sincronica

try{
    fs.unlinkSync('./perros.json');
}catch (err){
    console.log(err);
}


//Forma de Eliminar Asincronica

fs.unlink('./nuevoV2.txt', err=>{
    if(err){
        console.log('hubo un error borrando el archivo')
    }else{
        console.log('borrado con exito');
    }
})


//Crear una carpeta Forma Sincronica

//fs.mkdirSync('./nuevoV2');


//Crear una carpeta Forma Asincronica


/*fs.mkdir('./nuevoV2', err=>{

    if(err){
        console.log('hubo un error creando el directorio');
    }else{
        console.log('borrado el directorio con existo');
    }
});*/


//Leer archivo utilizando promesas

fs.promises.readFile('./nuevoV2.txt','utf-8')
    .then(contenido=>{
        console.log(contenido);
    })
    .catch(err=>{
        console.log('Error leyendo el archivo');
    })

//fs.promises.writeFile()
//fs.promises.unlink()
